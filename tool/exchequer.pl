#!/usr/bin/perl

use strict;
use warnings;

use XML::LibXML;
use Getopt::Long;
use List::Util qw(max);
use File::Basename;

GetOptions( 'debug' => \my $debug
          , 'tmpdir=s' => \my $tmpdir
          , 'csp-only' => \my $csp_only
          ) or die "Bad options";

# Find paths of required tools.
my $bindir = dirname($0);
my $xcsp2c = "$bindir/xcsp2c.pl";
my $cbmc = "$bindir/cbmc";
my $satsolver_name = "kissat";
my $satsolver = "$bindir/$satsolver_name";
if (!-e $cbmc) {
  my $sys_cbmc = `which cbmc`;
  chomp $sys_cbmc;
  $cbmc = $sys_cbmc if ($sys_cbmc =~ m/cbmc/ );
}
if (!-e $satsolver) {
  my $sys_satsolver = `which $satsolver_name`;
  chomp $sys_satsolver;
  $satsolver = $sys_satsolver if ($sys_satsolver =~ m/${satsolver_name}/ );
}

my $checker = "$bindir/xcsp3-tools-2.5.jar";

die "Couldn't find xcsp2c.pl at: $xcsp2c\n" if !-e $xcsp2c;
die "Couldn't find cbmc in path or at: $cbmc\n" if !-e $cbmc;
die "Couldn't find xcsp3-tools at: $checker\n" if !-e $checker;


# Print help if no instance given.
if (scalar @ARGV != 1) {
    print "Usage: ./exchequer.pl [--tmpdir=TMPDIR] [--csp-only] problem.xml\n";
    exit(0);
}

# Print out a comment.
sub comment($) {
    for my $line (split(/\n/, shift)) {
        print "c $line\n";
    }
}

if (-e $satsolver) {
    $cbmc .= " --external-sat-solver '$satsolver'";
}
else {
    comment("Couldn't find SAT solver in path or at: $satsolver");
    comment("Using CBMC's internal SAT solver instead.");
}

# Print version.
comment("Exchequer v1.1.0 by Martin Lester");

# Set up filenames for C file, log and solution.
my $instance_filename = $ARGV[0];
my $stub = basename($instance_filename);
$tmpdir = dirname($instance_filename) unless defined($tmpdir);

my $c_filename = "$tmpdir/$stub.c";
my $log_filename = "$tmpdir/$stub.log";
my $sol_filename = "$tmpdir/$stub.sol.xml";

-e $instance_filename or die "File $instance_filename does not exist.\n";

# Use xcsp2c to translate instance into C file.
comment("Running xcsp2c on: $instance_filename");
my $xcsp2c_out = `$xcsp2c '$instance_filename' '$c_filename' 2>&1`;
comment($xcsp2c_out);
-e $c_filename or die "Failed to create C file $c_filename.\n";

# Run CBMC on C file.
comment("Running CBMC on: $c_filename");
`$cbmc --trace '$c_filename' > '$log_filename'`;
comment("CBMC finished");

# Extract a solution from a CBMC log file and write it to an XML file.
sub extract_solution($$) {
    my $log = shift;
    my $sol = shift;

    my @solution = split /\n/, `grep "XCSP2C SOLUTION:" '$log'`;
    my @vars = ();
    my @vals = ();
    for my $s (@solution) {
        my ($var, $val) = ($s =~ m/SOLUTION: (\S+) = (\S+)/ );
        push @vars, $var;
        push @vals, $val;
    }

    open(my $s, ">", $sol) or die "Can't open $sol for writing: $!\n";
    print $s "<instantiation type=\"solution\">\n";
    print $s "  <list> ", join(" ", @vars), " </list>\n";
    print $s "  <values> ", join(" ", @vals), " </values>\n";
    print $s "</instantiation>\n";
    close($s) or die "Can't close $sol: $!\n";
}

# Check whether a log shows failed verification (so found a constraint solution).
sub cbmc_failed($) {
    my $log = shift;
    return `tail -1 '$log'` =~ m/VERIFICATION FAILED/ ;
}

# Check whether a log shows successful verification (so no constraint solution).
sub cbmc_succeeded($) {
    my $log = shift;
    `tail -1 '$log'` =~ m/VERIFICATION SUCCESSFUL/ 
}

# Check a solution in an XML file is valid using XCSP3 Tools.
# Return "true" if a solution to a CSP problem, "false" if invalid,
# or the objective value if a solution to a COP problem.
sub check_solution($) {
    my $sol = shift;
    my $checker_output = `java -cp '$checker' org.xcsp.parser.callbacks.SolutionChecker '$instance_filename' '$sol' 2>&1`;
    comment($checker_output);
    if ($checker_output =~ m/OK\s+$/ ) {
        return "true"
    }
    elsif ($checker_output =~ m/OK\s+(\d+)\s*$/ ) {
        return $1;
    }
    else {
        comment("Solution verification failed!");
        return "false";
    }
}

# If the first run of CBMC failed...
if (cbmc_failed($log_filename)) {

    # ...log it and extract and check the solution.
    comment("Solution found");
    extract_solution($log_filename, $sol_filename);
    comment("Running xcsp-tools solution verifier on: $sol_filename");
    
    my $res = check_solution($sol_filename);
    if ($res eq "true" || ($res ne "false" && $csp_only)) {
        # If it was a valid CSP solution, we are done.
        print "s SATISFIABLE\n";
        print `sed 's/^/v /' '$sol_filename'`;
        exit(0);
    }
    if ($res eq "false") {
        # If it wasn't valid, there is a bug. Give up.
        print "s UNKNOWN\n";
        exit(0);
    }

    # Otherwise, it was a valid solution to an optimisation problem.
    # Log that we found the first solution.
    `cp '$sol_filename' '$sol_filename.0'`;
    my @obj = ($res); # List of objectives in solutions.
    my $best = $#obj; # Index of current best solution.

    # Install signal handler to print best solution if interrupted.
    $SIG{TERM} = sub {
        comment("Caught SIGTERM; printing solution $best with objective $obj[$best]");
        print "s SATISFIABLE\n";
        print `sed 's/^/v /' '$sol_filename.$best'`;
        exit(0);
    };
    
    print "o $res\n";
    comment("Beginning optimisation");

    # Keep trying to find a better solution.
    while (1) {

        # Try to find a better solution by running CBMC again.
        my $x = scalar @obj; # Index of solution being found.
        my $target = $obj[-1]; # Target objective to beat.
        `$cbmc --trace -DTARGET=$obj[-1] '$c_filename' > '$log_filename.$x'`;

        if (cbmc_failed("$log_filename.$x")) {
            # If we found one...
            extract_solution("$log_filename.$x", "$sol_filename.$x");
            $res = check_solution("$sol_filename.$x");
            if ($res eq "false") {
                # ...if it was wrong, give up and show the best solution so far.
                $SIG{TERM} = 'IGNORE';
                print "s SATISFIABLE\n";
                print `sed 's/^/v /' '$sol_filename.$best'`;
                exit(0);
            }
            else {
                # ...if it was correct, log the improved objective.
                push @obj, $res;
                $best = $#obj;
                print "o $res\n";
            }
        }
        elsif (cbmc_succeeded("$log_filename.$x")) {
            # If we didn't find a better solution, we have found the optimum.
            # So print it out and quit.
            $SIG{TERM} = 'IGNORE';
            print "s OPTIMUM FOUND\n";
            print `sed 's/^/v /' '$sol_filename.$best'`;
            exit(0);
        }
        else {
            # If something went wrong with CBMC, quit and show the best validated solution.
            $SIG{TERM} = 'IGNORE';
            print "s SATISFIABLE\n";
            print `sed 's/^/v /' '$sol_filename.$best'`;
            exit(0);
        }
    }
}
elsif (cbmc_succeeded($log_filename)) {
    # If the first run of CBMC succeeded, the problem instance in unsatisfiable.
    print "s UNSATISFIABLE\n";
}
else {
    # If something went wrong with CBMC, give up.
    print "s UNKNOWN\n";
}

