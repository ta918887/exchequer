#!/usr/bin/perl

use strict;
use warnings;

use XML::LibXML;
use Getopt::Long;

use List::Util qw(max);

GetOptions( 'debug' => \my $debug

          ) or die "Bad options";

print "XCSP2C v1.1.0 by Martin Lester\n";

if (scalar @ARGV != 2) {
    print "Usage: ./xcsp2c.pl problem.xml problem.c\n";
    exit(0);
}

### SOME REGEXES AND CONSTANTS.

my $intre = qr{[\+\-]?\d+} ;
my $intrangere = qr{$intre\s*\.\.\s*$intre} ;
my $natre = qr{\d+} ;
my $varre = qr{\w+(?:\[$natre\])*} ;
my $intvarre = qr{$intre|$varre} ;
my $argre = qr{%\d+} ;
my $vargre = qr{%\.\.\.} ;

### READ AND CHECK INSTANCE HEADER.

# Read instance from XML.
my $instance_filename = $ARGV[0];
my $dom = XML::LibXML->load_xml(location => $instance_filename, huge => 1);

my $model_filename = $ARGV[1];
open(my $model, ">", $model_filename) or die "Can't open $model_filename for writing: $!\n";

# Check format (XCSP3) and whether CSP or COP.
my $format = $dom->findvalue("/instance/\@format");
die "Format is $format, not XCSP3.\n" unless $format eq "XCSP3";
my $type = $dom->findvalue("/instance/\@type");
die "Type is $type, not CSP or COP.\n" unless $type eq "CSP" || $type eq "COP";
my $better = "";

# Print start of C template.
print $model <<END;
#include <stdint.h>
#include <stdlib.h>
#include <assert.h>
void __CPROVER_printf(const char *format, ...);

#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))
END

if ($type eq "COP") {
    # Sense of relation is reversed, because we aim for assertion violation.
    $better = $dom->exists("instance/objectives/minimize") ? ">=" : "<=";
    print $model "#define BETTER $better\n";
}

print $model <<END;
int main() {

END

my $int_type = "int32_t";
my $flag_type = "int8_t";
my $logical_or = "|";
my $logical_and = "&";

### READ AND ENCODE VARIABLES.

my %array_bounds = ();

# Merge allowed ranges of a variable.
# Param example: "1 2 3 7 8 9"
# Return: ([1,3],[7,9])
sub merge_ranges($) {
    my @vals = ($_[0] =~ m/($intrangere|$intre)/g );
    my @ranges = ();

    if ($vals[0] =~ m/$intrangere/ ) {
        my ($x, $y) = ($vals[0] =~ m/($intre)\.\.($intre)/ );
        push @ranges, [$x, $y];
    }
    else {
        push @ranges, [$vals[0], $vals[0]];
    }
    
    for my $val (@vals) {
        if ($val =~ m/$intrangere/ ) {
            my ($x, $y) = ($val =~ m/($intre)\.\.($intre)/ );
            if ($x-1 <= $ranges[-1]->[1]) {
                $ranges[-1]->[1] = $y;
            }
            else {
                push @ranges, [$x, $y];
            }
        }
        else {
            if ($val-1 <= $ranges[-1]->[1]) {
                $ranges[-1]->[1] = $val;
            }
            else {
                push @ranges, [$val, $val];
            }
        }
    }
    return @ranges;
}

# Encode a variable.
# Params: variable name, XML node
# Effect: print C variable declaration, range assumption, output
sub encode_var($$);
sub encode_var($$) {
    my $id = shift;
    my $var = shift;

    if (defined($var->{"as"})) {
        my $proto = $var->{"as"};
        encode_var($id, ($dom->findnodes("instance/variables/var[\@id=\"$proto\"]"))[0]);
        return;
    }

    my @ranges = merge_ranges($var->to_literal);
    print $model "$int_type $id;\n";

    print $model "__CPROVER_assume(", join("||",
      map { "(($id >= $_->[0]) && ($id <= $_->[1]))" } @ranges), ");\n";

#    print $model "__CPROVER_output(\"$id\", $id);\n";
    print $model "__CPROVER_printf(\"XCSP2C SOLUTION: $id = %d\", $id);\n";
}

# Iterate over all variables in XML.
foreach my $var ($dom->findnodes("/instance/variables/var")) {
    my $id = $var->{"id"};
    encode_var($id, $var);
}

# Encode an array.
# Params: array name, XML node
# Effect: print C variable declaration, range assumption, output
sub encode_array($$);
sub encode_array($$) {
    my $id = shift;
    my $arr = shift;

    if (defined($arr->{"as"})) {
        my $proto = $arr->{"as"};
        encode_array($id, ($dom->findnodes("instance/variables/array[\@id=\"$proto\"]"))[0]);
        return;
    }

    my @dims = ($arr->{"size"} =~ m/\[($natre)\]/g );

    # Save array bounds for use in encoding later constraints.
    $array_bounds{$id} = [@dims];

    my @ranges = ();
    my %domain_ranges = ();
    my @domains = $arr->findnodes("domain");
    if (scalar @domains > 0) {
        for my $d (@domains) {
            my $f = $d->{"for"};
            if ($f eq "others") {
                @ranges = merge_ranges($d->to_literal);
            }
            else {
                my @r = merge_ranges($d->to_literal);
                for my $var (var_list($f)) {
                    $domain_ranges{$var} = \@r;
                }
            }
        }
    }
    else {
        @ranges = merge_ranges($arr->to_literal);
    }

    print $model "$int_type $id", (map {"[$_]"} @dims) , ";\n";

#    # Assume allowed ranges for every array member.
#    for (my $n = 0; $n <= $#dims; $n++) {
#        print $model "for (int _${id}_idx_$n = 0; _${id}_idx_$n < $dims[$n] ; _${id}_idx_$n++) {\n";
#    }
#    my $idx_id = $id . join("", map { "[_${id}_idx_$_]" } (0 .. $#dims));
#    print $model "__CPROVER_assume(", join("||",
#      map { "(($idx_id >= $_->[0]) && ($idx_id <= $_->[1]))" } @ranges), ");\n";
#
#    print $model "__CPROVER_printf(\"XCSP2C SOLUTION: $id", ("[%d]" x (scalar @dims)), " = %d\",",
#      join(", ", map { "_${id}_idx_$_" } (0 .. $#dims)), ", ",
#      $idx_id
#    ,");\n";
#    
#    for (my $n = 0; $n <= $#dims; $n++) {
#        print $model "}\n";
#    }

    my @idx = map { 0 } @dims;
    while (1) {

        my $var = $id . join("", map {"[$_]"} @idx);

        if (exists $domain_ranges{$var} || (scalar @ranges > 0)) {
            print $model "__CPROVER_assume(", join("||",
                map {
                    $_->[0] == $_->[1] ?
                    "($var == $_->[0])"
                    : "(($var >= $_->[0]) && ($var <= $_->[1]))"
                }
                (exists $domain_ranges{$var} ? @{$domain_ranges{$var}}: @ranges)
                ), ");\n";

            print $model "__CPROVER_printf(\"XCSP2C SOLUTION: $var = %d\", $var);\n";
        }

        for (my $n = $#dims; ; $n--) {
            return if $n < 0;
            $idx[$n]++;
            last unless $idx[$n] >= $dims[$n];
            $idx[$n] = 0;
        }
    }


}


# Iterate over all arrays in XML.
foreach my $arr ($dom->findnodes("/instance/variables/array")) {
    my $id = $arr->{"id"};
    encode_array($id, $arr);
}

### FUNCTIONS TO READ AND ENCODE CONSTRAINTS.

sub expand_arrays_ranges($) {
    my $arg = shift;

    if (!($arg =~ m/\[/ )) {
        if ($arg =~ m/($intre)\.\.($intre)/) {
            return ($1 .. $2);
        }
        elsif ($arg =~ m/($intre)x($intre)/) {
            return split(/ /, "$1 " x $2);
        }
        else {
            return $arg;
        }
    }

    my ($stem) = ($arg =~ m/(\w+)/ ) ;
    my @idx_raw = ($arg =~ m/\[([^\]]*?)\]/g );
    #my @idx_raw = ($arg =~ m/\[([\d\.]?)\]/g );
    my @idx_min = ();
    my @idx_max = ();

#print "arg: $arg\n";
#print "stem: $stem\n";
#print "idx_raw: ", join("#", @idx_raw), "\n";
    
    for (my $n = 0; $n <= $#idx_raw; $n++) {
        if ($idx_raw[$n] =~ m/($intre)\.\.($intre)/ ) {
            $idx_min[$n] = $1;
            $idx_max[$n] = $2;
        }
        elsif ($idx_raw[$n] eq "") {
            $idx_min[$n] = 0;
            $idx_max[$n] = $array_bounds{$stem}->[$n] - 1;
        }
        else {
            $idx_min[$n] = $idx_raw[$n];
            $idx_max[$n] = $idx_raw[$n];
        }
    }

#print "idx_min: ", join("#", @idx_min), "\n";
#print "idx_max: ", join("#", @idx_max), "\n";
    
    my @idx = @idx_min;
    my @elements = ();
    while (1) {
        my $element = $stem . join("", map {"[$_]"} @idx);
        push @elements, $element;
#print "element: $element\n";
        for (my $n = $#idx_raw; ; $n--) {
            return @elements if $n < 0;
            $idx[$n]++;
            last unless $idx[$n] > $idx_max[$n];
            $idx[$n] = $idx_min[$n];
        }
    }
}

sub var_list($) {
#print "varlist on: ", $_[0], "\n";
#    my @vars = (shift =~ m/(\w+|\w+\s*\[\s*\d*\s*\])/g );
#    map { $_ =~ tr/ // } @vars;

    my @vars = map {expand_arrays_ranges($_)} (shift =~ m/(\S+)/g );

#print "vars are", join("#", @vars), "\n";

    return @vars;
}

# Expand a range, such as 1..3.
# Param: Integer or range.
# Return: List of integers in range.
sub expand_range($) {
  return ($_[0]) unless $_[0] =~ m/$intrangere/ ;
  my ($left, $right) = ($_[0] =~ m/($intre)\s*\.\.\s*($intre)/ );
  return ($left .. $right);
}

# Remove leading/trailing whitespace from a string.
sub despace($) {
  my $full = shift;
  my ($cleaned) = ($full =~ m/^\s*(.*?)\s*$/ );
  return $cleaned;
}

# Parse a supports (or conflicts) expression.
# Param: String of supports expression.
# Return: List of tuples supported.
sub support_list($) {
    my $support = shift;

    # Unary case: no brackets; may have ranges.
    # TODO: Make this more efficient for large ranges.
    if (!($support =~ m/\(.*?\)/ )) {
        my @vals = ($support =~ m/($intrangere|$intre)/g );
        @vals = map {expand_range($_)} (@vals);
        return (map {[$_]} @vals);
    }

    # General case: brackets; no ranges.
    my @tuples = ($support =~ m/\((.*?)\)/g );
    my @all_tuples = ();
    for my $tuple (@tuples) {
        push @all_tuples, [map {despace($_)} (split(/,/, $tuple))];
    }

    return @all_tuples;
    
    # TODO: Compress tables and make the encoding more efficient:
    # https://stackoverflow.com/questions/5810649/finding-rectangles-in-a-2d-block-grid
    # https://web.archive.org/web/20150921112543/http://www.drdobbs.com/database/the-maximal-rectangle-problem/184410529
}

# Encode a supports constraint.
sub extension_supports ($$) {
    my @vars = var_list(shift);
    my @supports = support_list(shift);

    # Encode each supported tuple.
    my @tuples = ();
    foreach my $tuple (@supports) {
        my @atoms = map {$tuple->[$_] eq "*" ? () :
            "($vars[$_] == $tuple->[$_])"
        } (0 .. $#vars);
        next if scalar @atoms == 0;
        push @tuples, "(" . join(" $logical_and ", @atoms) . ")";
    }
    
    return if scalar @tuples == 0;
    
    # Join all the tuples: one must hold.
#    print $model "    __CPROVER_assume(\n    ", join(" ||\n    ", @tuples), "\n    );\n";
    
    print $model "{\n";
    print $model "$flag_type _flag = 0;\n";
    print $model map {"_flag = _flag $logical_or $_ ;\n"} @tuples;
    print $model "__CPROVER_assume(_flag);\n";
    print $model "}\n";
    
}

# Encode a conflicts constraint.
sub extension_conflicts ($$) {
    my @vars = var_list(shift);
    my @supports = support_list(shift);

    my @tuples = ();
    foreach my $tuple (@supports) {
        # Difference from supports: tuple is forbidden.
        my @atoms = map {$tuple->[$_] eq "*" ? () :
            "($vars[$_] != $tuple->[$_])"
        } (0 .. $#vars);
        next if scalar @atoms == 0;
        push @tuples, "(" . join(" $logical_or ", @atoms) . ")";
    }
    
    return if scalar @tuples == 0;

    # Difference from supports: all must not hold.
#    print $model "    __CPROVER_assume(\n    ", join(" &&\n    ", @tuples), "\n    );\n";

    print $model map {"__CPROVER_assume($_);\n"} @tuples;

}


sub operate ($$$) {
    my %ops2c = (
        "add" => "+",
        "sub" => "-",
        "mul" => "*",
        "div" => "/",
        "mod" => "%",
        "dist" => "-",
        "lt" => "<",
        "le" => "<=",
        "ge" => ">=",
        "gt" => ">",
        "ne" => "!=",
        "eq" => "=="
    );
#print "operate: ", $_[0], " ",  $_[1], " ", $_[2], "\n";
    my $d = $_[0] eq "dist";
    return "(" . ($d ? "abs(" : "") . $_[1] . " " . $ops2c{$_[0]} . " " . $_[2] . ($d ? ")" : "") . ")";
}

sub encode_intension ($) {
    
    my $func = shift;
    if ($func =~ m/(\w+)\((\w+)\(($intvarre),($intvarre)\),($intvarre)\)/ ) {
        print $model "__CPROVER_assume(", operate($1,operate($2,$3,$4),$5), ");\n";
        return;
    }
    elsif ($func =~ m/(\w+)\(($intvarre),(\w+)\(($intvarre),($intvarre)\)\)/ ) {
        print $model "__CPROVER_assume(", operate($1,$2,operate($3,$4,$5)), ");\n";
        return;
    }
    elsif ($func =~ m/(\w+)\(($intvarre),($intvarre)\)/ ) {
        print $model "__CPROVER_assume(", operate($1,$2,$3), ");\n";
        return;
    }
    die "Unsupported function expression: $func\n";
}

sub encode_alldifferent ($) {
    my @vars = var_list(shift);
    
    for my $x (0 .. $#vars) {
        for my $y ($x+1 .. $#vars) {
            print $model "__CPROVER_assume(", $vars[$x], "!=", $vars[$y], ");\n";
        }
    }
}

sub encode_sum ($$$) {
#print "encode sum: #", $_[0], "#",$_[1],"#",$_[2],"#\n";
    my @vars = var_list(shift);
    my @coeffs = var_list(shift // "");
    my ($operator, $operand) = (shift =~ m/\((\w+),($intvarre)\)/ );

#print "vars: ", join("#", @vars), "\n";
#print "coeffs: ", join("#", @coeffs), "\n";
#print "sizes: ", (scalar @vars), " ", (scalar @coeffs), "\n";    
    push @coeffs, split(/ /, "1 " x ((scalar @vars)-(scalar @coeffs)));

    # Could replace this with use of operand.
    my %ops2c = (
        "lt" => "<",
        "le" => "<=",
        "ge" => ">=",
        "gt" => ">",
        "ne" => "!=",
        "eq" => "=="
    );

    my @term = ();
    for my $x (0 .. $#vars) {
        if ($coeffs[$x] == 1) {
            push @term, $vars[$x];
        }
        else {
            push @term, $coeffs[$x] . "*" . $vars[$x];
        }
    }
    
    print $model "__CPROVER_assume((", join(" + ", @term), ") ", $ops2c{$operator}, " ", $operand, ");\n";
}

sub encode_element($$$) {

#print "ops from: ", $_[2], "\n";

    my @vars = var_list(shift);
    my $index = despace(shift);
    my ($operator, $operand) = (shift =~ m/\((\w+),($intvarre)\)/ );

#print "vars: ", join("#", @vars), "\n";
#print "index: $index\n";
#print "operator/operand: $operator $operand\n";
    
    for my $x (0 .. $#vars) {
        print $model "__CPROVER_assume(($index != $x) || (", operate($operator, $vars[$x], $operand) , "));\n";
    }
}

### EXPANSION OF TEMPLATES TO FIRST ORDER CONSTRAINTS.

sub param_idx($) {
    return substr($_, 1);
}


sub substitute($$) {
    my $sources_ref = shift;
    my @sources = @{$sources_ref};
    my $xml = shift;

    # Does the source template use %... ?
    my $vargs = (grep { m/$vargre/ } @sources) > 0;
    # How many parameters in the source template?
    my @raw_params = map { m/($argre)/g } @sources;
    my $max_param = max((-1, map {param_idx($_)} @raw_params));

    my @templates = ();

    # For each argument in the group:
    for my $arglist ($xml->findnodes("args")) {
        # Extract the arguments.
        my @args = ($arglist->to_literal =~ m/(\S+)/g );

        # Expand out any ranges (1..2) or arrays (x[1][]).
        @args = map {expand_arrays_ranges($_)} @args;

        # Make a new copy of the source template.
        my @template = ();
        # For each element of the source template:
        for my $source (@sources) {
            my $t = $source;
            # Substitute every parameter.
            for my $x (0 .. $max_param) {
                my $arg = $args[$x];
                # Lookahead to make sure %10 doesn't match %1.
                $t =~ s/%$x(?!\d)/$arg/g ;
            }
            # Substitute %... too, if necessary.
            if ($vargs) {
                my $arg = join(" ", @args[($max_param + 1) .. $#args]);
                $t =~ s/$vargre/$arg/g ;
            }
            push @template, $t;
        }
        push @templates, [@template];
    }
    
    return @templates;
    
}


### ITERATE OVER AND ENCODE ALL CONSTRAINTS.

## SINGLE CONSTRAINTS.

# EXTENSION/SUPPORTS (SINGLE)
foreach my $support ($dom->findnodes("//extension[not(ancestor::group)]/supports/..")) {
    extension_supports($support->findvalue("list"), $support->findvalue("supports"));
}

# EXTENSION/CONFLICTS (SINGLE)
foreach my $conflict ($dom->findnodes("//extension[not(ancestor::group)]/conflicts/..")) {
    extension_conflicts($conflict->findvalue("list"), $conflict->findvalue("conflicts"));
}

# INTENSION (SINGLE)
foreach my $intension ($dom->findnodes("//intension[not(ancestor::group)]")) {
    encode_intension($intension->findvalue("function") || $intension->to_literal);
}

# ALLDIFFERENT (SINGLE)
foreach my $alldiff ($dom->findnodes("//allDifferent[not(ancestor::group)]")) {
    encode_alldifferent($alldiff->findvalue("list") || $alldiff->to_literal);
}

# SUM (SINGLE)
foreach my $sum ($dom->findnodes("//sum[not(ancestor::group)]")) {
    encode_sum($sum->findvalue("list"), $sum->findvalue("coeffs"), $sum->findvalue("condition"));
}

# ELEMENT (SINGLE)
foreach my $element ($dom->findnodes("//element[not(ancestor::group)]")) {
    encode_element($element->findvalue("list"), $element->findvalue("index"),
        ($element->findvalue("condition") || "(eq," . despace($element->findvalue("value")) . ")" )
    );
}

## GROUP CONSTRAINTS.

# EXTENSION/SUPPORTS (GROUP)
foreach my $support ($dom->findnodes("//group/extension/supports/../..")) {
    my @templates = substitute(
        [$support->findvalue("extension/list"), $support->findvalue("extension/supports")],
        $support
    );

    for my $template (@templates) {
        extension_supports($template->[0], $template->[1]);
    }
}

# EXTENSION/CONFLICTS (GROUP)
foreach my $conflict ($dom->findnodes("//group/extension/conflicts/../..")) {
    my @templates = substitute(
        [$conflict->findvalue("extension/list"), $conflict->findvalue("extension/conflicts")],
        $conflict
    );

    for my $template (@templates) {
        extension_conflicts($template->[0], $template->[1]);
    }
}

# INTENSION (GROUP)
foreach my $intension ($dom->findnodes("//group/intension/..")) {
    my @templates = substitute(
        [$intension->findvalue("intension/function") || $intension->findvalue("intension")],
        $intension
    );

    for my $template (@templates) {
        encode_intension($template->[0]);
    }
}

# ALLDIFFERENT (GROUP)
foreach my $alldiff ($dom->findnodes("//group/allDifferent/..")) {
    my @templates = substitute(
        [$alldiff->findvalue("allDifferent/list") || $alldiff->findvalue("allDifferent")],
        $alldiff
    );

    for my $template (@templates) {
        encode_alldifferent($template->[0]);
    }
}

# SUM (GROUP)
foreach my $sum ($dom->findnodes("//group/sum/..")) {
    my @templates = substitute(
        [$sum->findvalue("sum/list"), $sum->findvalue("sum/coeffs"), $sum->findvalue("sum/condition")],
        $sum
    );

    for my $template (@templates) {
        encode_sum($template->[0], $template->[1], $template->[2]);
    }
}

# ELEMENT (GROUP)
foreach my $element ($dom->findnodes("//group/element/..")) {
    my @templates = substitute(
        [$element->findvalue("element/list"), $element->findvalue("element/index"),
        ($element->findvalue("element/condition") ||
        "(eq," . despace($element->findvalue("element/value")) . ")" )
        ],
        $element
    );

    for my $template (@templates) {
        encode_element($template->[0], $template->[1], $template->[2]);
    }
}

## OBJECTIVE FOR OPTIMISATION.

print $model "#ifdef TARGET\n" if $type eq "COP";
if ($type eq "COP") {

    my $obj = ($dom->findnodes("instance/objectives/minimize"), $dom->findnodes("instance/objectives/maximize"))[0];


print "obj is: ", $obj, "\n";

    my $obj_type = $obj->{"type"} // "expression";
    if ($obj_type eq "expression") {
        print $model "assert(", $obj->to_literal, " BETTER TARGET);\n";
    }
    else {
        my $list_value = $obj->exists("list") ? $obj->findvalue("list") : $obj->to_literal;
        my $coeffs_value = $obj->findvalue("coeffs");
        
        my @vars = var_list($list_value);
print "vars: @vars\n";
        my @coeffs = var_list($coeffs_value);
        push @coeffs, split(/ /, "1 " x ((scalar @vars)-(scalar @coeffs)));

        my @term = ();
        for my $x (0 .. $#vars) {
            if ($coeffs[$x] == 1) {
                push @term, $vars[$x];
            }
            else {
                push @term, $coeffs[$x] . "*" . $vars[$x];
            }
        }
        
        if ($obj_type eq "sum") {
            print $model "assert(", join(" + ", @term), " BETTER TARGET);\n";
        }
        elsif (($obj_type eq "minimum") || ($obj_type eq "maximum")) {
            my $choose = $obj_type eq "minimum" ? "MIN(" : "MAX(";
            if (scalar @term == 1) {
                print $model "assert(", $term[0], " BETTER TARGET);\n";
            }
            else {
                print $model "$int_type _obj = $term[0];\n";
                for my $t (@term[1 .. $#term]) {
                    print $model "_obj = $choose _obj, $t);\n";
                }
                print $model "assert(_obj BETTER TARGET);\n";
#                print $model "assert($choose",
#                    join(", $choose", @term[0..($#term-1)]),
#                    ", " , $term[-1], (")" x (scalar $#term)), " BETTER TARGET);\n";
            }
        }
        elsif ($obj_type eq "nValues") {
            print $model "assert(";
            for my $x (0 .. ($#term-1)) {
                print $model "(";
                print $model join(" && ", map {"($term[$x] != $_)"} (@term[($x+1) .. $#term]));
                print $model ") + ";
            }
            print $model "1 BETTER TARGET);\n";
        }
        else {
            print STDERR "Unsupported objective type: $obj_type\n";
        }
        
    }

}
print $model "#endif\n" if $type eq "COP";

# Print end of C template.
print $model <<END;
#ifndef TARGET
    assert(0);
#endif
}
END

close $model or die "Can't close $model_filename: $!\n";

