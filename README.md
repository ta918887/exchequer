Exchequer v1.2.0
================

Exchequer is a solver for constraint satisfaction problems (CSP) and
constraint optimisation problems (COP) expressed in the XCSP3-Core format. 
This version is submitted to the Mini Solver tracks of the XCSP3 Competition
2024. Only a subset of XCSP3-Core is supported.

Exchequer v1.0.1 won the Mini CSP track of the XCSP3 Competition 2022.
Exchequer v1.1.0 won the Mini CSP track of the XCSP3 Competition 2023 and
came 2nd in the Mini COP track.

The solver works by translating an XCSP3 instance into a C program,
which violates an assertion only if the values of the variables in the
program give a solution to the instance. Then it uses the bounded model-checker
CBMC to attempt to verify absence of assertion violations. If CBMC finds an
assertion violation, it reports back a counterexample trace, which Exchequer
turns into an instance solution. If it finds no assertion violation,
Exchequer reports that the instance is unsatisfiable.

CBMC itself works by translating a C program into a giant SAT instance,
which it solves using a SAT solver. For this release, we have included a
build of CBMC that can use an external SAT solver. For the SAT solver, we
use Kissat 3.1.1. On harder problems, this performs slightly better than
CaDiCaL (used in Exchequer 1.0.1) and significantly better than MiniSAT
(default solver in older versions of CBMC).

It is unlikely that Exchequer's approach will ever be better than a direct
encoding in SAT (for example, as implemented by PicatSAT). It was written
primarily to demonstrate that its approach is feasible, and to see how much
worse it is.

Exchequer's translation is quite naive. It can easily generate very large C
files, for example, if the instance contains a large extension constraint
that is used as a template. In this case, even if the instance is solved,
most of the time is spent by CBMC generating the SAT instance, not running
the SAT solver.

Exchequer is implemented as two Perl scripts: xcsp2c.pl performs the
translation, while exchequer.pl is a wrapper that calls xcsp2c.pl and CBMC.
Exchequer also uses XCSP3 Tools to validate solutions before returning them.

Be warned that xcsp2c.pl's parsing and error-checking is not very robust. If
you give it an instance with an unsupported constraint, it will silently
ignore it. If the format of a supported constraint is not what it expects,
it may crash, generate an invalid C file, or generate a valid C file that
does not correctly encode the constraint.

For constraint optimisation (COP) problems, Exchequer simply calls CBMC
repeatedly, trying to find a solution incrementally better than the previous
one each time. When a better solution cannot be found, we know this is the
optimum. The actual value of the objective for each solution is calculated
using XCSP3 Tools.

Usage
-----

````
perl DIR/tool/exchequer.pl --tmpdir=TMPDIR BENCHNAME
````

Where:

* `DIR` is the extracted archive directory;
* `BENCHNAME` is the XML file encoding the XCSP3 instance;
* `TMPDIR` is the optional temporary directory to use.

If no temporary directory is given, Exchequer will use the directory
containing the instance. In any case, it will write the following files:

* a `.c` file encoding the instance;
* a `.log` file recording the output from CBMC;
* a `.sol.xml` file containing the solution.

Libraries
---------

Exchequer was tested with the following libraries from Debian GNU/Linux 12
(Bookworm), which are not included in the distribution, but any relatively
recent version should work:

* Perl v5.36.0
* Perl XML::LibXML 2.0134
* OpenJDK 17.0.11 2024-04-16

If you use Debian or a derived distribution, you can get these by running:
`apt install openjdk-17-jre libxml-libxml-perl`

If you use CentOS, you can get these by running:
`yum install java-17-openjdk perl-XML-LibXML`

Exchequer is bundled with executables for the following:

* CBMC version 5.95.1 64-bit x86_64 linux --- https://github.com/diffblue/cbmc/archive/refs/tags/cbmc-5.95.1.tar.gz
* Kissat 3.1.1 --- https://github.com/arminbiere/kissat/archive/refs/tags/rel-3.1.1.tar.gz
* XCSP3 Tools 2.5 --- https://repo1.maven.org/maven2/org/xcsp/xcsp3-tools/2.5/xcsp3-tools-2.5.jar

If you do not want to use the bundled version of CBMC, you can `apt install
cbmc` to get Exchequer working (and it will find it in your path), but be
aware that older versions do not support using an external solver and
usually use the inferior MiniSAT solver.

You can also compile Kissat and/or CBMC yourself. The executables bundled
were compiled statically as follows:

Kissat was compiled with the following commands, which disable UNSAT proof
logging for a slight speed/memory improvement:

````
./configure --competition --no-proofs --quiet -static
make
````

CBMC was compiled with the following commands:

````
cmake -S . -Bbuild -DWITH_JBMC=OFF -DBUILD_SHARED_LIBS=OFF -DCMAKE_CXX_FLAGS=-static
cmake --build build
````

The build fails, but not until after the CBMC executable has been compiled.

Both executables (`build/kissat` and `build/bin/cbmc`) need to be copied
into Exchequer's `tool` directory.

Licensing
---------

Exchequer, Kissat and XCSP3 Tools are distributed under the MIT License.

CBMC is distributed under a BSD-style 4-clause licence; see
LICENSE-cbmc.txt.

Changelog
---------

* 1.2.0 --- minor updates for XCSP3 2024 competition:
  * CBMC updated to 5.95.1.
  * XCSP3 tools updates to 2.5.
  * Kissat updated to 3.1.1. (Considered SBVA-CaDiCaL, SAT Competition 2023
    winner, but it was slightly slower on the XCSP 2023 Mini CSP instances.)
* 1.1.0 --- various small fixes and improvements for XCSP3 2023 competition:
  * Fix bugs in encoding of some objectives for COP problems.
  * Updated CBMC bundled; reduces memory consumption for some problems.
  * Use Kissat (SAT Competition 2022 bulky version) as SAT solver with CBMC.
  * Turn on XML parser library flag for large text nodes used in some problems.
  * Use bitwise rather than logical AND/OR in some conditions; performance
    typically stays the same or improves very slightly.
* 1.0.1 --- fix some bugs and add COP support for XCSP3 2022 competition.
* 1.0.0 --- initial release.

Possible Improvements
---------------------

* Try using smaller integer types where possible. For example, if a problem
  only has 0/1 variables and extension constraints, there is no need to use
  int32_t for the variables. (Once arithmetic is involved through intension
  constraints, this becomes harder to do safely.)
* Better encoding of large extension constraints. There have been several
  papers published on this topic. It might be possible to implement some of
  the techniques in xcsp2c.
* Better error handling. At the moment, unrecognised constraints are mostly
  ignored. It would be better to give an error.
* Handle all of XCSP3 Core. At the moment, xcsp2c uses regexes to parse the
  limited range of constraints allowed in the XCSP3 Competition Mini Solver
  tracks, so this would require a substantial rewrite with a proper parser
  and handling of compound expressions.
* Investigate whether a more complex search strategy would improve
  performance on optimisation. We could try a binary search, possibly with
  multiple runs of CBMC in parallel to tighten bounds from both sides. This
  would reduce the number of SAT instances to solve, at the expense of
  having to solve several UNSAT cases (which tend to be harder than SAT
  instances), rather than just one.
* Avoid calling CBMC so many times for optimisation problems. It ought to be
  possible to reuse the same SAT encoding and (for example) invoke a MaxSAT
  solver. Without access to CBMC internals, this is tricky, but could
  probably be done with two runs as follows: Dump SAT instance in CNF with
  CBMC. Run MaxSAT solver to find optimal solution. Run CBMC with dummy
  solver that passes the solution found.
* Investigate whether other bounded model checkers work better than CBMC.
  CBMC spends a lot of time on static analysis of the program before
  conversion to SAT. A faster, more stupid model checker might skip this,
  possibly giving a less efficient SAT encoding, but that might not matter.
  Alternatively, ESBMC 7.4's interval analysis with SMT might work better
  for problems with larger integers.

